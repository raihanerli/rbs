<section class="roberto-about-area section-padding-100-0">
    <!-- Hotel Search Form Area -->
    <div class="hotel-search-form-area">
        <div class="container-fluid">
            <div class="hotel-search-form">
                <form action="#" method="post">
                    <div class="row justify-content-around align-items-end">
                        <div class="col-6 col-md-2">
                            <label for="checkIn">Check In</label>
                            <input type="date" class="form-control" id="checkIn" name="checkin-date">
                        </div>
                        <div class="col-6 col-md-2">
                            <label for="checkOut">Check Out</label>
                            <input type="date" class="form-control" id="checkOut" name="checkout-date">
                        </div>
                        <div class="col-4 col-md-2">
                            <label for="room">Ruangan</label>
                            <select name="room" id="room" class="form-control">
                                <option value="01">Aula Atas</option>
                                <option value="02">Aula Bawah</option>
                            </select>
                        </div>
                        <div class="col-4 col-md-2">
                            <label for="room">Fasilitas</label>
                            <select name="room" id="room" class="form-control">
                                <option value="01">Lapangan Futsal</option>
                                <option value="02">Tepas FKIP</option>
                            </select>
                        </div>
                        <div class="col-12 col-md-3">
                            <button type="submit" class="form-control btn roberto-btn w-100">Check Availability</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container mt-100">
        <div class="row align-items-center">
            <div class="col-12 col-lg-6">
                <!-- Section Heading -->
                <div class="section-heading wow fadeInUp" data-wow-delay="100ms">
                    <h6>About Us</h6>
                    <h2>Welcome to <br>Universitas Islam Nusantara</h2>
                </div>
                <div class="about-us-content mb-100">
                    <h5 class="wow fadeInUp" data-wow-delay="300ms">Universitas Islam Nusantara atau dikenal dengan sebutan UNINUS merupakan salah satu perguruan tinggi Islam swasta tertua & bersejarah di Jawa Barat yang berlokasikan di Jl. Soekarno - Hatta No. 530 Kota Bandung.</h5>
                    <p class="wow fadeInUp" data-wow-delay="400ms">Rektor: <span>Prof. Dr. H. Obsatar Sinaga, S.IP., M.Si.</span></p>
                    <img src="{{ asset('img/core-img/signature.png')}}" alt="" class="wow fadeInUp" data-wow-delay="500ms" style="height: 50px;">
                </div>
            </div>

            <div class="col-12 col-lg-6">
                <div class="about-us-thumbnail mb-100 wow fadeInUp" data-wow-delay="700ms">
                    <div class="row no-gutters">
                        <div class="col-6">
                            <div class="single-thumb">
                                <img src="{{ asset('img/bg-img/kelas.jpg')}}" alt="">
                            </div>
                            <div class="single-thumb">
                                <img src="{{ asset('img/bg-img/mesjid.jpg')}}" alt="">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="single-thumb">
                                <img src="{{ asset('img/bg-img/gedung-uninus.jpg')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
