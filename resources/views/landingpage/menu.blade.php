<div class="classy-menu">
                            <!-- Menu Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>
                            <!-- Nav Start -->
                            <div class="classynav navbar navbar-expand-lg">
                                <ul id="nav ">
                                    <li class="active"><a href="#">Home</a></li>
                                    <li><a href="{{ url('/list')}}">Ruangan</a></li>
                                    <li><a href="{{ url('/contact')}}">Contact Us</a></li>
                                    </ul>
                                 <!-- Book Now -->
                                 <div class="book-now-btn ml-3 ml-lg-5">
                                    <a href="#">Book Now <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                </div>
                                <!-- Search -->
                                <div class="search-btn ml-4">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </div>
                                @guest
                                <ul>
                                    <li><a href="{{ route('login') }}">Login</a></li>
                                    <li><a href="{{ route('register') }}">Register</a></li>
                                </ul>
                                @else
                                <ul>
                                <li><a href="#">{{ Auth::user()->name }}</a>
                                        <ul class="dropdown">
                                        <li><a href="{{ url('admin')}}">Kelola User</a></li>
                                        <li><a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">{{ __('Logout')}}</a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                @csrf
                                            </form></li>
                                    </ul>
                                    </li>
                                </ul>
                                @endguest
                            </div>
                            <!-- Nav End -->
                        </div>
