@extends('admin.index')

@section('content')
    <div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">Form input Ruangan</h4>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form action="{{ route('ruangan.store') }}" method="POST" class="forms-sample" enctype="multipart/form-data">
                    @csrf
                  <div class="form-group row">
                    <label for="exampleInputUsername2" class="col-sm-3 col-form-label">Nama Ruangan</label>
                    <div class="col-sm-9">
                      <input type="text" name="nama" class="form-control" id="exampleInputUsername2" placeholder="Nama Ruangan">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="exampleSelectGender"  class="col-sm-3 col-form-label">Kategori Ruangan</label>
                    <div class="col-sm-9">
                        <select class="form-control" id="exampleSelectGender" name="kategori_ruangan_id">
                            <option>-- Pilih Kategori Ruangan --</option>
                            @foreach ($kategori_ruangan as $kat)
                            <option value="{{ $kat->id }}">{{ $kat->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    </div>
                    <div class="form-group row">
                        <label for="exampleSelectGender"  class="col-sm-3 col-form-label">Gedung</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="exampleSelectGender" name="gedung_id">
                                <option>-- Pilih Kategori Gedung --</option>
                                @foreach ($gedung as $gd)
                                <option value="{{ $gd->id }}">{{ $gd->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        </div>
                  <div class="form-group row">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Lantai</label>
                    <div class="col-sm-9">
                        <input type="text" name="lantai" class="form-control" placeholder="Lantai">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Kapasitas</label>
                    <div class="col-sm-9">
                        <input type="text" name="kapasitas" class="form-control" placeholder="Kapasitas">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="exampleInputMobile" class="col-sm-3 col-form-label">Foto 1</label>
                    <div class="col-sm-9">
                        <input type="file" name="foto1" class="form-control" placeholder="Foto 1">
                    </div>
                  </div>
                  <div class="form-check form-check-flat form-check-primary">
                  </div>
                  <button type="submit" name="proses" class="btn btn-primary mr-2">Submit</button>
                  <a href="{{ url('ruangan')}}" class="btn btn-warning text-white">Batal</a>
                </form>
              </div>
            </div>
          </div>
    </div>
    </div>
@endsection
