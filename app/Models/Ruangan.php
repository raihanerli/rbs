<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
    use HasFactory;
    protected $table = 'ruangan';
    protected $guarded = ['id'];

    // database relationn many to one gedung
    public function gedung()
    {
        return $this->belongsTo(Gedung::class);
    }
    // database relation many to one kategori ruangan
    public function kategoriRuangan()
    {
        return $this->belongsTo(KategoriRuangan::class);
    }
}
