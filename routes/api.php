<?php

use App\Http\Controllers\GedungController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('gedung', [GedungController::class, 'index']);
// Route::get('gedung/show/{id}', [GedungController::class, 'show']);
// Route::get('gedung/destroy/{id}', [GedungController::class, 'destroy']);
// Route::post('gedung/store', [GedungController::class, 'store']);
// Route::post('gedung/update/{id}', [GedungController::class, 'update']);
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
